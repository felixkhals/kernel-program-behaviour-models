# dependencies
- build is tested on a ubuntu
- for qemu minimum needed: `sudo apt install qemu qemu-system-x86`

# build
## create an image file with a debian rootfs (only needed first time)

run `./create_image.sh`, this downloads dbootstrap and installs a debian into a image file

## chroot into debian rootfs to build the kernel
(build on your system is possible, but for example ubuntu 20.04 libs throw exceptions)

run `./chroot_image.sh`, this drops you in a shell on the created debian

to build:
```bash
apt update
apt-get install -y fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex libelf-dev bison
cd /mnt
./build_kernel.sh
```

## ~~chroot into debian rootfs to build test modules~~
> This is historical information, please note that this uses kernel threads.
> Our perf events are currently configured not to count kernel space.
> **This and possibly other changes cause this not to work**. We are using pb_submitter for testing.

run `./chroot_image.sh`, this drops you in a shell on the created debian and run

```bash
/mnt/build_module.sh
```

## chroot into debian rootfs to build pb_submitter and test programs
run `./chroot_image.sh`, this drops you in a shell on the created debian and run

```bash
/mnt/build_pb_submitter.sh
```

# run the build kernel with the created rootfs
run `./run_qemu.sh`

# debug using gdb
run `./run_qemu.sh gdb`

run in a seperate shell:
```bash
cd .. # to workspace root

gdb vmlinux \
    -ex 'lx-symbols' \
    -ex 'target remote :1234' \
    -ex 'hb init_pb_rq'
```

look for gdb `add-auto-load-safe-path` warning and follow instruction to enable loading of kernel gdb scripts

use hardware breakpoints instead of software breaks

# Troubleshooting

## `GPG error: ... KEYEXPIRED` on `apt update`

In case the `apt update` throws an error due to an error like this:
```shell
GPG error: http://archive.debian.org lenny/updates Release: The following signatures were invalid: KEYEXPIRED 1356982504
```

The problem is most probably an OS version (in this case an old debian) with expired keys.
The keys can not be updated (as far as i know/tried) but the issue can be circumvented:

### Solution

[source](https://stackoverflow.com/questions/29070471/gpg-error-http-archive-debian-org-lenny-updates-release-the-following-signat/43259335#43259335)

Just change the system date
```shell
date --set 2008-01-01
```
then try to update
```shell
apt update
```
After that, do not forget to reset/sync the current date! [source](https://askubuntu.com/questions/81293/what-is-the-command-to-update-time-and-date-from-internet/998449#998449)
```shell
sudo timedatectl set-ntp off
sudo timedatectl set-ntp on

```

## How to read debootstrap logs if e.g. chroot failed?

1. Mount the created image (e.g. on Fedora: right mouse button > open with > mount)
2. Use your favourite file manager to access the mounted image
3. Go to and read debootstrap/debootstrap.log

