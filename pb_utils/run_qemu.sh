#!/bin/bash
MODE=$1
TEST_IMAGE=build/qemu-image.img
KERNEL=../arch/x86/boot/bzImage
IMAGE_OPTIONS=" -drive file=$TEST_IMAGE,index=0,media=disk,format=raw"
DEBUG_OPTIONS=" -s -S"

COMMAND="qemu-system-x86_64"
[ "$MODE" == "gdb" ] &&  COMMAND+=$DEBUG_OPTIONS
COMMAND+=" -kernel $KERNEL -cpu host"
COMMAND+=$IMAGE_OPTIONS
# linux kernel parameters: single user mode, disable kaslr for better gdb debugging
COMMAND+=" -append \"root=/dev/sda rootwait rw single console=ttyS0 nokaslr\""
COMMAND+=" --enable-kvm"
COMMAND+=" --nographic"
COMMAND+=" --smp 1"
COMMAND+=" -net nic -net user"
COMMAND+=" -monitor none"

eval $COMMAND
