#!/bin/bash
IMG=build/qemu-image.img
DIR=build/mount-point.dir

mkdir $DIR


sudo mount -o loop $IMG $DIR    # mount image
sudo mount --bind ../ $DIR/mnt  # mount git into image

sudo chroot $DIR

sudo umount $DIR/mnt
sudo umount $DIR
rmdir $DIR
