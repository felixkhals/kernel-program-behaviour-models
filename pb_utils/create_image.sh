#!/bin/bash
IMG=build/qemu-image.img
DIR=build/mount-point.dir
KEYRING=build/debian-keyring.gpg
DEBIAN_VERSION=jessie
DEBOOTSTRAP_FOLDER=$PWD/build/debootstrap
DEBOOTSTRAP_SOURCE=https://deb.debian.org/debian/pool/main/d/debootstrap
MIRROR=https://archive.debian.org/debian-archive/debian

# get dbootstrap
if [ ! -d $DEBOOTSTRAP_FOLDER ]; then
    mkdir -p $DEBOOTSTRAP_FOLDER
    # get versions sorted ascendingly by last modification (?C=M;O=A supported by apache servers
    # which makes something like sort -V unnecessary which also can not take the modification date
    # into account but just the filename) and just take the last/latest entry
    currentVersion=$(wget -O- $DEBOOTSTRAP_SOURCE/?C=M\;O=A 2> /dev/null | \
        egrep -o 'debootstrap_[0-9\.]+[^_]*_all.deb' | tail -1)
    wget -O $DEBOOTSTRAP_FOLDER/$currentVersion $DEBOOTSTRAP_SOURCE/$currentVersion 2> /dev/null
    ar -xf --output $DEBOOTSTRAP_FOLDER $DEBOOTSTRAP_FOLDER/$currentVersion
    # unpack into target dir (see https://wiki.ubuntuusers.de/tar/#Extrahieren)
    tar xzf $DEBOOTSTRAP_FOLDER/data.tar.gz -C $DEBOOTSTRAP_FOLDER --strip-components=1
    tar xzf $DEBOOTSTRAP_FOLDER/control.tar.gz -C $DEBOOTSTRAP_FOLDER --strip-components=1
    rm $DEBOOTSTRAP_FOLDER/data.tar.gz $DEBOOTSTRAP_FOLDER/control.tar.gz
fi

mkdir -p $DIR

qemu-img create $IMG 2g
mkfs.ext2 $IMG

sudo mount -o loop $IMG $DIR

# download (retired) old debian jessie key (see https://ftp-master.debian.org/keys.html)
KEY=release-8.asc
wget -O $DEBOOTSTRAP_FOLDER/$KEY https://ftp-master.debian.org/keys/$KEY 2> /dev/null
#rsync -az --progress keyring.debian.org::keyrings/keyrings/debian-keyring.gpg "$DEBOOTSTRAP_FOLDER/debian-keyring.gpg"
# actually download and build the debian image using debootstrap (the mirror is required since
# debian 8 is not listed in the main repo anymore but only in the archive)
sudo DEBOOTSTRAP_DIR="$DEBOOTSTRAP_FOLDER/usr/share/debootstrap" $DEBOOTSTRAP_FOLDER/usr/sbin/debootstrap --verbose --keyring $DEBOOTSTRAP_FOLDER/$KEY --arch amd64 "$DEBIAN_VERSION" $DIR $MIRROR

sudo umount $DIR
rmdir $DIR

