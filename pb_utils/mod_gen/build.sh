#!/bin/bash

if [ ! -d "/mnt/pb_utils/mod_gen" ]; then
    echo "are you running this in chroot or the build system?"
    exit 1
fi

cd /mnt/pb_utils/mod_gen/
rm -rf mods
./plan_to_module.pl
cd mods && make

echo "################################################"
echo "modules created and copied to /root"
echo "use 'insmod <file' to run within the test system"
