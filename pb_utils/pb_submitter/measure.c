#include <stdio.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>

// address of the syscall in the kernel syscall table
#define BEHAVE_SET_ROOT_PROC 3141

static void usage(void)
{
    fputs("Usage: ./measure <prog_name> <argument_1> ...", stderr);
}

int main(int argc, char** argv)
{
    int ret;

    if (argc < 2) {
        usage();
        return -1;
    }

    // start the perf measuring
    ret = syscall(BEHAVE_SET_ROOT_PROC);
    if (ret != 0) {
        fputs("BEHAVE_SET_ROOT_PROC failed", stderr);
        return -1;
    }

    /**
     * BEWARE:
     * We also measure the execve call etc. which does not really
     * belong to the program but as long as we do it in the forecast model
     * (here) as well as in the runtime model the resulting models should be
     * rather similiar).
     */

    // pass the root proc pid to be able to be read using e.g. a bash pipe
    char buffer[16];
    snprintf(buffer, sizeof(buffer), "%d", getpid());
    write(1 /*STDOUT*/, buffer, strlen(buffer));

    /**
     * BEWARE:
     * Since we use the STDOUT to pass arguments to the a following program we
     * must make sure that the measured program itself does NOT write to STDOUT
     * but only to STDERR!
     */

    // drop the first element of argv (the name of this program) so that only
    // the name and arguments of the program to run remain
    char ** args = &argv[1];

    // replace this process with the chosen program since we want to keep using
    // the same PID for simplicity purposes (forking a new process would have
    // worked as well but this process is not needed anymore anyways)
    ret = execvp(argv[1], args);

    if (ret < 0) {
        perror("execve");
        return -1;
    }

    return 0;
}
