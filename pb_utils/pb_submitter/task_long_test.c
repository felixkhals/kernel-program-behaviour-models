#include <stdio.h>
#include <unistd.h>

int main(void)
{
    int i;
    int j;

    // Make sure program is not finished before pb scheduler takes control
    sleep(1);

    // this outer for loop is around 8 instructions per iteration
    for (i = 0; i < 100; i++) {
        // this for loop results in 4 cpu instructions per iteration
        // nop, add, compare, jmp
        for (j = 0; j < 10000000; j++){
            __asm__ volatile("nop");
        }

        // check if program runs && syscall to switch tasks
        printf("run #%d completed\n", i);
        usleep(1);
    }

    printf("main end\n");
    return 0;
}
