#!/bin/bash

if [ ! -d "/mnt/pb_utils/pb_submitter" ]; then
    echo "are you running this in chroot or the build system?"
    exit 1
fi

cd /mnt/pb_utils/pb_submitter
gcc -static -o pb_submitter pb_submitter.c
gcc -static -o test_prog test_prog.c
gcc -static -o task_long task_long_test.c
gcc -static -o sysc_long syscall_long_test.c
gcc -static -o measure measure.c

cp pb_submitter measure test_prog task_long sysc_long example_run.sh example_plan /root

echo "All done. Run '/root/example_run.sh' within ./run_qemu.sh now"
