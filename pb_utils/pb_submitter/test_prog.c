#include <stdio.h>
#include <unistd.h>
#include <sys/prctl.h>

int main(void)
{
    int i, j;
    int status = 0;
    pid_t wpid;

    // Creating first child
    pid_t n1 = fork();

    // Creating second child. First child
    // also executes this line and creates
    // grandchild.
    pid_t n2 = fork();

    // this for loop results in 4 cpu instructions per iteration
    // nop, add, compare, jmp
    for (j = 0; j < 1000000; j++){
        __asm__ volatile("nop");
    }

    // check if program runs && syscall to switch tasks
    printf("run completed\n");
    usleep(1);

    // barrier: make all fathers wait for all its child processes (since all processes use that line)
    while ((wpid = wait(&status)) > 0) {};

    if (n1 > 0 && n2 > 0)
    {
        fprintf(stderr, "This is the parent %d (%d, %d)\n", getpid(), n1, n2);

        // syscall to switch tasks
        usleep(1);
    }
    else if (n1 == 0 && n2 > 0)
    {
        // set a different process name (comm) for the child to differentiate it from the parent
        prctl(PR_SET_NAME, "test_child_1");
        fprintf(stderr, "This is the first child %d (%d, %d)\n", getpid(), n1, n2);
    }
    else if (n1 > 0 && n2 == 0)
    {
        prctl(PR_SET_NAME, "test_child_2");
        fprintf(stderr, "This is the second child %d (%d, %d)\n", getpid(), n1, n2);
    }
    else {
        prctl(PR_SET_NAME, "test_child_3");
        fprintf(stderr, "This is the third child %d (%d, %d)\n", getpid(), n1, n2);
    }
    return 0;
}
