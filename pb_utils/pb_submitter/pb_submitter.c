#include <stdio.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h> // errno

#define PB_SET_PLAN 0x1337

static void usage(void)
{
    puts("Usage: echo '<ref_proc_pid>' | ./pb_submitter <prog_name> <prog_arg_0> ...");
}

volatile sig_atomic_t stop;

static void handler(int n)
{
    stop = 1;
}

int main(int argc, char** argv)
{
    int ret;

    pid_t ref_proc;
    errno = 0;

    char buf[12]; // large enough buffer for even the largest pid
    scanf("%11s", buf); // read what we have in stdin and drop the ending null
    if (sscanf(buf, "%u", &ref_proc) != 1) {
        printf("Error: Invalid stdin input (pid expected): %s; Errno: \n", strerror(errno));
        usage();
        return -1;
    }
    printf("Got: %u\n", ref_proc);

    // TODO Add the reference proc pid as argument (arc < 3)?
    if (argc < 2) {
        usage();
        return -1;
    }

    pid_t pid = fork();

    if (pid == 0x0) {
        // drop the first element of argv (the name of this program) so that only
        // the name and arguments of the program to run remain
        char ** args = &argv[1];

        puts("    ######   STOP");
        kill(getpid(), SIGTSTP);
        puts("    ######   CONTINUE");

        // replace this process with the chosen program since we want to keep using
        // the same PID for simplicity purposes (forking a new process would have
        // worked as well but this process is not needed anymore anyways)
        ret = execvp(argv[1], args);
        if (ret < 0) {
            perror("execve");
            return -1;
        }
    } else {
        /**
         * Wait for execve of child to finish else pb cycles are wasted inside
         * the execve function.
         *
         * Setup a signal handler for the SIGCHLD-Signal that the parent receives when the child is
         * stopped (by SIGTSTP)
         *
         * idea for the signal handler:
         * https://stackoverflow.com/questions/40098170/handling-sigtstp-signals-in-c/40116030#40116030
         * idea for using it in combination with pause() instead of wait:
         * https://stackoverflow.com/questions/12683466/after-suspending-child-process-with-sigtstp-shell-not-responding/43614414#43614414
         */
        struct sigaction sa;
        sa.sa_handler = handler;
        sa.sa_flags = 0;
        sigemptyset(&sa.sa_mask);
        if (sigaction(SIGCHLD, &sa, NULL) == -1) {
            perror("Couldn't set SIGCHLD handler");
            exit(EXIT_FAILURE);
        }
        // wait for the child to signal the stop
        pause();

        printf("Selected plan (reference pid): %u, real pid: %u\n", ref_proc, pid);
        ret = syscall(PB_SET_PLAN, ref_proc, pid);

        // continue running the child
        kill(pid, SIGCONT);

        if (ret < 0) {
            puts("PB_SET_PLAN failed");
            return -1;
        }

        // continue running the child
        kill(pid, SIGCONT);
    }

    return 0;
}
