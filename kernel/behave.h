#ifndef PLAN_BASED_LINUX_SCHEDULER_BEHAVE_H
#define PLAN_BASED_LINUX_SCHEDULER_BEHAVE_H

#include <linux/types.h>
#include "sched/perf_error_detection.h"

/******************************************************************************
* Based on "libpbm":
*  Program Behaviour Model (PBM) as a Task Precedence Graph (TPG),
*  implemented as a Acyclic Directed Graph (ADG) structure. Using
*  Linux' perf_event interface for task performance measurement.
*  Author: Michael Zent
*  Context: Softwareproject 'Cluster Management', Lecturer: Barry Linnert, SS 2022 FU Berlin
*/

// node types
#define FORK 1
#define JOIN 2
#define COMP 3 // computation
#define COMM 4 // communication (not supported yet)

/*
* PBM node, describing a program task
*/
typedef struct _pbm_NODE
{
    uint8_t type;      // FORK, JOIN, or COMP (COMM not supported yet)
    int32_t thread_id; // ID of the current thread within its thread group

    /*
    * Performance count value, interpretation depends on type
    * FORK - Number of forked threads
    * JOIN - Number of joined threads
    * COMP - Number of instructions needed to complete the task
    * COMM - Total length of all messages sent, in byte (not supported yet)
    */
    uint64_t count;

    /*
    * Inter-node connectors
    * children - First child in a list of children. There should be >= 1 children
    *            only if type == FORK, else a node has just one child.
    * next_sib - Next sibling of a node. Should be != NULL only if the node is a
    *            child of a FORK node, with exception of the last child.
    */
    struct _pbm_NODE* children; // first child (in a list of children)
    struct _pbm_NODE* next_sib; // next sibling

    // marker for graph traversion
    uint8_t visited;
} pbm_NODE;

/*
* Program Behavior Model (PBM)
*/
typedef struct _PBM
{
    pbm_NODE* root; // first task of a thread // TODO Rename to first or first_task?
    pbm_NODE* last; // current last task

    // the task nodes of the parent which are used as time markers/dates to
    // know where the task graph must be inserted in the parent task-graph in
    // the post-processing stage
    pbm_NODE* fork_date; // fork task of the parent process
    pbm_NODE* exit_date; // current/last task of the parent while this child exited

    /*
    * The Fork Buffer contains pointers to PBMs which describe the forked
    * child threads. Should be != NULL with size > 0 only after forking.
    */
    struct _PBM* children; // first child (in a list of forked children)
    struct _PBM* next_sib; // next sibling

    // number of children (and children's children) it totally spawned
    uint64_t child_count;

    /*
    * Performance measurement and recording
    */
    struct perf_event* pevent;
} PBM;

/*
* Creates a new PBM and initializes it.
* Should be the first PBM-function called.
*
* Returns a pointer to that PBM, or NULL on error.
*/
PBM* pbm_create(void);

/*
* Deletes the given PBM and frees the associated resources.
* Should only be called if pbm_create() was called prior, and
* if no pbm_task_start() or pbm_fork() remained unclosed.
*/
void pbm_destroy(PBM* pbm);

/*
* Inserts into the given PBM a new node of given type (for now only
* COMP, as COMM in not supported yet) and starts performance counting.
* Should be called immediately before the task starts, and be closed
* by pbm_task_end().
*
* Returns 0 on error, 1 on full success, or 2 if performance counting
* could not start.
*/
int pbm_task_start(PBM* pbm, uint8_t type, struct task_struct* proc);

/*
* Ends performance counting for the last node of the given PBM and
* records the results.
* Should be called immediately after the task ends, and as the next
* PBM-method after pbm_task_start().
*
* Returns 0 on failure, i.e. the performance counts could not be
* recorded, otherwise returns 1.
*/
int pbm_task_end(PBM* pbm);

 pbm_NODE* pbm_fork_parent_new_task(struct task_struct* parent_proc);

/*
* Inserts into the given PBM a FORK node which may have up to
* 'num_thr' children, describing the forked child threads. If
* num_thr == 0 the max number of threads is determined auto-
* matically via OpenMP's omp_get_max_threads().
* Should be called immediately before the thread is forked, and be
* closed by pbm_join().
*
* Returns 0 on error, or 1 on success.
*
* The process as an explicit argument is needed since the fork is called by the parent.
*/
int pbm_fork(struct task_struct* proc, pid_t parent_pid, pbm_NODE* fork_node);

int pbm_exit(pid_t pid, pid_t parent_pid);

/*
* Inserts into the given PBM a JOIN node. Records the actual number of
* forked threads. Merges the sub-PBMs, describing the child threads, from
* the fork buffer into the parent PBM and releases the fork-buffer.
* Should be called immediately after the child threads are joined, and as
* the next PBM-method after pbm_fork().
*
* Returns 0 on error, or 1 on success.
*/
int pbm_join(PBM* child_pbm);

/*
* Crude recursive ADG printer. Starts with the given node, which
* should be the root node of the given PBM.
*/
void pbm_print_graph(PBM* pbm, pbm_NODE* node);

void pbm_join_and_print_graph_self(pid_t pid);

/*
* Auxiliary methods, not for public usage. Nomen est omen.
*/
pbm_NODE* _pbm_create_node(uint8_t type, pid_t pid);
void _pbm_destroy_node(pbm_NODE* node);
void _pbm_unvisit_node(pbm_NODE* node);

/*
* Initialize the map necessary to locate the PBM by given PID
* 
* BEWARE: Forgetting to call this before calling fork() will trigger
* Segmentation faults!
*/
void pbm_init(void);
void pbm_uninit(void);

/******************************************************************************/

bool is_root_process(struct task_struct* p);
bool is_relevant_process(struct task_struct *p);

int start_counting(struct task_struct *p);
int stop_counting(void);

/******************************************************************************/

PBM* get_pbm_by_pid(pid_t pid);

/******************************************************************************
 * Plan runtime state
 *
 * Represents the current state of the plan at runtime.
 * The head of the stack should always be the currently executed process and corresponding plan
 * node.
 *
 * Implementation:
 * Two stacks which store the plan-node and the corresponding runtime-process
 * accumulated by forking and should be executed from top to bottom (current process is always the
 * head).
 * This works since we assume that a fork in the plan always corresponds to a runtime fork which
 * means that a fork node that we push to the stack as a start for the traversal after the next
 * exit-node should always have a corresponding runtime-process which should be run when the
 * previous (child) process exits.
 */
struct plan_rt_state {
	pbm_NODE* node_stack[PROC_BUF_SIZE];
	struct task_struct* proc_stack[PROC_BUF_SIZE];
	size_t stack_size;

    // number of exited processes of the plan needed to measure of the plan is finished
    u64 num_exited_procs;
};

void plan_rt_state_init(void);
struct task_struct* plan_rt_state_peek_proc(void);
void plan_rt_state_peek(pbm_NODE** node_res, struct task_struct** proc_res);
int plan_rt_state_is_empty(void);
void plan_rt_state_push(pbm_NODE* node, struct task_struct* proc);
void plan_rt_state_pop(void);
void plan_rt_state_debug_print(void);
void plan_rt_state_incr_num_exited_procs(void);
u64 plan_rt_state_num_exited_procs(void);

#endif //PLAN_BASED_LINUX_SCHEDULER_BEHAVE_H
