#include <linux/seq_file.h>
#include <linux/proc_fs.h>
#include "perf_error_detection.h"
#include <linux/syscalls.h>
#include <linux/spinlock.h>
#include <linux/perf_event.h>
#include <linux/kthread.h>
#include <linux/list.h>
#include "sched.h"

typedef struct pb_plan pb_plan_t;

// terminal colors (source: https://pkg.go.dev/github.com/whitedevops/colors)
#define Red "\033[91m"
#define Yellow "\033[33m"
#define Cyan "\033[36m"
#define Bold "\033[1m"
#define End "\033[0m"


static void reset_triggering_syscall_info(void) {
	struct pb_rq *pb = &(this_rq()->pb);
	pb->triggering_syscall.type = sched_trig_OTHER;
	pb->triggering_syscall.origin = NULL;
	pb->triggering_syscall.target = NULL;
}


/* -------------------------------------------------------------------------- */

SYSCALL_DEFINE2(pb_set_plan, pid_t, reference_proc, pid_t, root_proc) {
	struct task_struct* task;
	struct rq* rq;
	struct pb_rq* pb_rq;

	int res;

	PBM* pbm;

	task = find_task_by_vpid(root_proc);
	if (!task) {
		return -1;
	}

	pbm = get_pbm_by_pid(reference_proc);

	printk(KERN_WARNING "Init Plan RunTime state\n");
	// reset plan runtime state
	plan_rt_state_init();

	// prepare the plan runtime stack by pushing the root node of the reference model/plan
	plan_rt_state_push(pbm->root, task);

	rq = this_rq();

	// prevent the pb-scheduler from being informed of the fork of the root process since we want
	// it to contiue running instead of getting switched
	reset_triggering_syscall_info();

	// Only the root process has the scheduling class manually set since it is already initialized
	// for using the fair scheduler in the fork() syscall in pb_submitter which must happen before
	// initializing the plan since knowledge of the PID of the root process is necessary to init
	// the performance counting as soon as possible.
	// BEWARE: This also means that the scheduling class of this root process needs to be reset to
	// the fair scheduler on exit so that it can do the necessary cleanup in its data structures.
	task->sched_class = &pb_sched_class;

	pb_rq = &rq->pb;

	pb_rq->root_proc = task;
	pb_rq->num_exited_procs = pbm->child_count;

	res = pb_submit_plan(rq);

	if (res == -1) {
		printk("pb_submit_plan == -1\n");
		return res;
	}

	return 0;
}

/*
 * It is possible submit a plan only if no plan is currently executed
 */
int pb_submit_plan(struct rq *rq)
{
	/*
	 * Must be volatile to ensure correct initialization order
	 */
	volatile struct pb_rq * pb = (volatile struct pb_rq*)(&(rq->pb));
	int error;
	int i = 0;

	if (pb->mode != PB_DISABLED_MODE) {
		return -1;
	}

	error = init_perf_event_into_map(plan_rt_state_peek_proc(), 0);
	if(error) {
		//initialization error detection/handling could happen here
		printk(KERN_WARNING "PB INIT,%u: FAILED OPEN PERF EVENT\n", i);
	} else {
		printk(KERN_DEBUG "PB INIT,%u\n", i);
	}

	pb->count_pb_cycles = 0;
	pb->count_admin_cycles = 0;
	pb->total_instr = 0;

	pb->is_initialized = 1;	// must be initialized last

	resched_curr(rq);	// reschedule ASAP

	return 0;
}
EXPORT_SYMBOL(pb_submit_plan);

// called by core.c sched_init
void init_pb_rq(struct pb_rq *pb_rq)
{
	pb_rq->n_pb_cycles = 100;
	pb_rq->count_pb_cycles = 0;
	pb_rq->n_admin_cycles = 20;
	pb_rq->count_admin_cycles = 0;
	pb_rq->mode = PB_DISABLED_MODE;
	pb_rq->is_initialized = 0;

	init_pid_2_pevent_map();
}
EXPORT_SYMBOL(init_pb_rq);

// IO has finished, we can schedule the next task
/**
 * This is called in activate_task() in wake_up_new_task() which gets called during _do_fork() 
 * while switching from parent- to child-context. __schedule() may be called after it depending on
 * various conditions (see __schedule in kernel/sched/core.c) but therefore it is always called 
 * before dequeue_task_pb() and pick_next_task_pb().
 */
static void enqueue_task_pb(struct rq *rq, struct task_struct *p, int flags)
{
	struct pb_rq *pb = &(rq->pb);

	*(waiting_on_io_by_pid(p->pid)) = 0;

	if (rq->pb.mode == PB_EXEC_MODE) {
		printk(KERN_WARNING "ENQEUE TASK %u\n", p->pid);
		if (pb->triggering_syscall.type == sched_trig_FORK) {
			pbm_NODE* cur_node;
			pbm_NODE* fork_node;
			struct task_struct* cur_proc;
			int fork_node_type;

			// safe current process for later use since the plan_rt_state might get modified
			plan_rt_state_peek(&cur_node, &cur_proc);
			fork_node = cur_node->children;
			fork_node_type = fork_node->type;

			/**
			* the scheduling class (pb) of the forked child is set in kernel/sched/core.c:sched_fork()
			*/

			// if a fork occured then the next node should be a fork node
			if (FORK != fork_node_type) {
				printk(KERN_WARNING "ERROR: Fork node expected but got: %i\n", fork_node_type);
				//TODO: Delegate to higher instance
			}

			/**
			* since we prepend the child node in pbm_fork() (see behave.c) the child of a
			* fork-node is the child node (->children) while the parent is the next sibling
			* (->next_sib).
			*/

			// update the parent node: Keep the process and replace the node before the fork
			// with the _parent_ node after it
			// Precondition: The plan_rt_state is not empty (since pb_set_plan() initialized it)
			plan_rt_state_pop();
			plan_rt_state_push(fork_node->children->next_sib, pb->triggering_syscall.origin);

			// add the child
			plan_rt_state_push(fork_node->children, pb->triggering_syscall.target);
			plan_rt_state_debug_print();

			// reset the info so that the next relevant triggering syscall can be detected again
			reset_triggering_syscall_info();

			// force rescheduling so that the child can be picked nextZ
			resched_curr(rq); //FIXME TESTING!!!
		}
	}
}

/**
 * This is called right AFTER enqueue_task() in wake_up_new_task().
 */
static void check_preempt_curr_pb(struct rq *rq, struct task_struct *p, int flags)
{
	// NOP
}

/**
 * task started IO and thus it is finished
 *
 * This is called in __schedule() by deactivate_task() BEFORE pick_next_task() which means the
 * state of the plan (e.g. in case of an exit) is not yet updated!
 */
static void dequeue_task_pb(struct rq *rq, struct task_struct *p, int flags)
{
	struct pb_rq *pb = &(rq->pb);
	u64 perf_counter;
	u64 counter_diff;
	u64 read_error;
	u64 expected_instr_count;
	u64 diff_from_expected;
	pbm_NODE* cur_node;
	struct task_struct* cur_proc;
	struct perf_event* pevent;

	bool process_exited = false;

	//printk("Dequeue task: %u\n", p->pid);

	if (*(waiting_on_io_by_pid(p->pid))/* && p->state != TASK_DEAD*/) {
		printk("Waiting for IO\n");
		return;
	}
	*(waiting_on_io_by_pid(p->pid)) = 1;

	// safe current process for later use since the plan_rt_state might get modified
	plan_rt_state_peek(&cur_node, &cur_proc);
	pevent = get_pevent_by_pid(cur_proc->pid);
	
	if(!pevent) {
		printk("WARNING: PERF EVENT IS NULL");
	}

	if (pb->triggering_syscall.type == sched_trig_EXIT) {
		// remove the exited process from the stack and run the next available
		plan_rt_state_pop();
		process_exited = true;

        // only mark the process as exited if it is not expected to do more tasks
        // (if it would still have child tasks it would be a premature exit)
        if (cur_node && !cur_node->children) {
            plan_rt_state_incr_num_exited_procs();
        }

		//terminate_perf_event(get_pevent_by_pid(prev_proc->pid));
		plan_rt_state_debug_print();

		// reset the info so that the next relevant triggering syscall can be detected again
		reset_triggering_syscall_info();
	} else {
		if (!plan_rt_state_is_empty()) {
			plan_rt_state_debug_print();
		}
	}

	if (cur_node) {
		expected_instr_count = cur_node->count;
	} else {
		expected_instr_count = 0;
	}

	// printk(KERN_ALERT "DEBUG: Passed %s %d \n",__FUNCTION__,__LINE__);
	read_error = get_perf_counter(pevent, &perf_counter);
	if (read_error) {
		printk(KERN_WARNING "FETCHING PERFORMANCE COUNTER IN PB SCHEDULER FAILED WITH %llu\n", read_error);
	}
	counter_diff = perf_counter; //- pb->total_instr;
	pb->total_instr = perf_counter;
	diff_from_expected = abs(counter_diff - expected_instr_count);
	//TODO: Set proper threshold for significance (relative values would probably be better than absolutes)
	//if (diff_from_expected > 0)
	{
		printk(KERN_WARNING Yellow Bold "PB TASK %u RAN %llu / %llu INSTRUCTIONS TOO %s" End "\n",
               cur_proc->pid, diff_from_expected, expected_instr_count,
				counter_diff < expected_instr_count ? "SHORT" : "LONG");
	}

	//TODO: Can this actually happen? Can a process die without calling exit?
	// remove a dead process which has not called exit from the plan
	if (!process_exited && cur_proc->state == TASK_DEAD) {
		plan_rt_state_pop();
	}

	if (is_plan_finished(pb)) {
		if (!is_plan_successful(pb)) {
			printk(KERN_WARNING "PLAN TERMINATED PREMATURELY \n");
		}
		else {
			printk(KERN_WARNING Bold Yellow "PLAN DONE" End "\n");
		}

		// set back to cfs for completion of task
		pb->is_initialized = 0;
	}
	printk(KERN_WARNING Bold Yellow "Exited: %i .Proc state: %li (?= %i)" End "\n", process_exited, cur_proc->state, TASK_DEAD);

	if (process_exited && pb->root_proc == cur_proc) {
		cur_proc->sched_class = &fair_sched_class;
		resched_curr(rq);
	}

		/*
		// show all current processes (source: https://unix.stackexchange.com/questions/299140/linux-is-there-a-way-to-dump-the-task-run-queue/336663#336663)
		{
			struct task_struct *process, *thread;
			int cnt = 0;

			rcu_read_lock();
			for_each_process_thread(process, thread) {
				task_lock(thread);

				// exclude system processes (which have been started earlier and thereby have a lower pid)
				if (thread->pid > 1000) {
					printk(KERN_WARNING "%li, %u, %s, %s\n", thread->state, thread->pid, thread->comm,
							thread->sched_class == &fair_sched_class ? "fair" :
								(thread->sched_class == &pb_sched_class ? "pb" :
									"other"));
				}
				task_unlock(thread);
				cnt++;
			}
			rcu_read_unlock();
		}
		*/
}

static void task_dead_pb(struct task_struct *p)
{
	/**
	 * We cant put the exit notification here because this only gets called _after_ a switch from a
	 * dead process to another one.
	 * For such a context switch it would be required that a new process is chosen which only
	 * happens when the plan runtime state is updated but that update requires the information that
	 * should be emitted here (which closes the circular dependency).
	 * Therefore it must be done e.g. in do_exit() and not here.
	 */
}

static struct task_struct * pick_next_task_pb(struct rq *rq,
		struct task_struct *prev, struct rq_flags *rf)
{
	// contains task to be executed
	struct task_struct *picked = NULL;
	enum pb_mode current_mode, next_mode;
	struct pb_rq *pb = &(rq->pb);
	
	current_mode = pb->mode;
	next_mode = determine_next_mode_pb(rq);
	pb->mode = next_mode;

	if (next_mode == PB_DISABLED_MODE && current_mode == PB_EXEC_MODE) {
		// After Plan is done do the cleanup
		// FIXME: This should be done for all processes on exit and not just for the last one in the plan?!
		//terminate_perf_event(&(get_pevent_by_pid(plan_rt_state_peek_proc()->pid)));
		// TODO: Check if we have to free the memory or if perf takes care of it
		// see 'perf_event_release_kernel(struct perf_event *event)' in core.c
	}
	/**
	 * This handles the case where the program to be run is dead before the
	 * pb scheduler starts executing
	 */
	if (current_mode == PB_DISABLED_MODE && current_mode != next_mode) {
		if (!is_plan_finished(pb) && (plan_rt_state_peek_proc() ? (plan_rt_state_peek_proc()->state == TASK_DEAD) : true)) {
			pb->mode = PB_DISABLED_MODE;
			next_mode = PB_DISABLED_MODE;
			picked = NULL;
			pb->is_initialized = 0;
			printk(KERN_WARNING "PLAN TERMINATED PREMATURELY \n");
		}
	}

	if (current_mode != next_mode) {
		printk("SWITCHING MODES: %i -> %i\n", current_mode, next_mode);
		pb->count_admin_cycles = 0;
		pb->count_pb_cycles = 0;
		// Push last non-plan task back in its corresponding runqueue
		if (next_mode == PB_EXEC_MODE) {
			// Necessary to manage the preempted task
			printk("PUT OLD TASK BACK IN RQ\n");
			put_prev_task(rq, prev);  // TODO: This does nothing, right? So why even call?
		}
	}

	// EXEC Mode is next, so we return our next task to be executed
	if (next_mode == PB_EXEC_MODE) {

		// printk(KERN_ALERT "DEBUG: Passed %s %d \n",__FUNCTION__,__LINE__);
		if(current_mode == PB_ADMIN_MODE) {
			printk(KERN_DEBUG "PB ADMIN,STOP,%llu\n", sched_clock());
		} else if (current_mode == PB_DISABLED_MODE) {
			printk("Switching from disabled to EXEC\n");
		}

		picked = plan_rt_state_peek_proc();
	}

	return picked;
}

static void yield_task_pb(struct rq *rq)
{
	// NOP
}

static void put_prev_task_pb(struct rq *rq, struct task_struct *p)
{
	// NOP
}

static void set_curr_task_pb(struct rq *rq)
{
	// NOP
}

/*
 * TODO: Make sure this does't interrupt determine_next_mode_pb() and pick_next_task_pb()
 */
static void task_tick_pb(struct rq *rq, struct task_struct *p, int queued)
{
	struct pb_rq *pb = &(rq->pb);

	if (pb->mode != PB_EXEC_MODE) {
		return;
	}

	pb->count_pb_cycles++;

	// printk("TICK #%d\n",pb->count_pb_cycles);

	if (determine_next_mode_pb(rq) != PB_EXEC_MODE && pb->mode == PB_EXEC_MODE) {
		//printk("Reschudling in task_tick_pb");
		resched_curr(rq);
	}
}

static unsigned int get_rr_interval_pb(struct rq *rq, struct task_struct *task)
{
	return 0;
}

static void prio_changed_pb(struct rq *rq, struct task_struct *p, int oldprio)
{
	// NOP
}

static void switched_to_pb(struct rq *rq, struct task_struct *p)
{
	// NOP
}

static void update_curr_pb(struct rq *rq)
{
	// NOP
}

const struct sched_class pb_sched_class = {
	.next			= &dl_sched_class, // arrange the pb scheduler to be higher prioritized than the deadline (dl) scheduler
	.enqueue_task		= enqueue_task_pb,
	.dequeue_task		= dequeue_task_pb,
	.yield_task		= yield_task_pb,

	.check_preempt_curr	= check_preempt_curr_pb, // NOP

	.pick_next_task		= pick_next_task_pb,
	.put_prev_task		= put_prev_task_pb, // NOP

	//.task_dead		= task_dead_pb, // NOP
	.set_curr_task          = set_curr_task_pb, // NOP
	.task_tick		= task_tick_pb,

	.get_rr_interval	= get_rr_interval_pb, // NOP (return 0)

	.prio_changed		= prio_changed_pb, // NOP
	.switched_to		= switched_to_pb, // NOP

	.update_curr		= update_curr_pb, // NOP
};
EXPORT_SYMBOL(pb_sched_class);
