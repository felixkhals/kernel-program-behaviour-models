#include "perf_error_detection.h"

/*
 * Our understanding of perf so far. Please correct as needed.
 *
 * An event in perf's terms is an instruction in our case.
 * A sample is a fixed number of events that CAN trigger a wakeup_event (but not the only way).
 * An overflow event is triggered by a fixed number of wakeup_event.
 *
 */

/*
 * initialize perf event for new task
 */
int init_perf_event(struct task_struct* proc, u64 num_instr,
					struct perf_event** pevent)
{
	unsigned long irq_flags;
    struct perf_event_attr pea; // config info for the perf_event interface

	/*
    * Configure the performance counter
    */
    memset(&pea, 0, sizeof(struct perf_event_attr));
	pea.type = PERF_TYPE_HARDWARE;
	pea.size = sizeof(struct perf_event_attr);
	pea.config = PERF_COUNT_HW_INSTRUCTIONS;
	pea.sample_period = num_instr;
	pea.disabled = 0; 		// start the counter as soon as we're in userland
	pea.exclude_kernel = 1; // only count user space
	pea.exclude_hv = 1; // excluding events that happen in the hypervisor

	printk(KERN_WARNING "TASK: %u, CPU: %i, PTR: %llu\n", proc->pid, smp_processor_id(), (u64)proc);

    /*
    * Try to enable the performance counter
    */
	// disable irqs to make 'perf_event_ctx_activate' in 'kernel/events/core.c' happy
	local_irq_save(irq_flags);
	*pevent = perf_event_create_kernel_counter(
        &pea,
        -1,   // measure on all cores (in case the process runs on different ones)
        proc, // exclusively measure the forked process (BEWARE: a process can only measure itself!)
        NULL, // overflow_handler disabled, because we count within the scheduler
        NULL
    );
	local_irq_restore(irq_flags);

	if (IS_ERR(*pevent)) {
			printk(KERN_WARNING "TASK: %u | PB ERROR INITIALISING PERF EVENT: %li\n", proc->pid, PTR_ERR(*pevent));
            // cast to prevent compiler warnings
            if (-EOPNOTSUPP == (int64_t)*pevent) {
                printk(KERN_WARNING
                    "TASK: %u | EOPNOTSUPP (-95): The hardware does not support certain attributes! "
                    "E.g. perf_event_attr.precise_ip > 0 may not be supported.\n", proc->pid);
            }
            if (-EINVAL == (int64_t)*pevent) {
                printk(KERN_WARNING
                    "TASK: %u | EINVAL (-22): Invalid argument! "
                    "E.g. CPU with given index does not exist.\n", proc->pid);
            }

            if (-ESRCH == (int64_t)*pevent) {
                printk(KERN_WARNING
                    "TASK: %u | ESRCH (-3): No such process! "
                    "E.g. the process to measure already exited.\n", proc->pid);
            }
            return -1;
	}
    if ((*pevent)->state != PERF_EVENT_STATE_ACTIVE) {
		printk(KERN_WARNING "TASK: %u | Event is inactive", proc->pid);
	}

	return 0;
}


/*
 * Must not be called after the event is terminated.
 */
u64 get_perf_counter(struct perf_event *pevent, u64 *perf_counter)
{
    int read_error = perf_event_read_local(pevent, perf_counter);
	if (read_error) {
		printk(KERN_WARNING "TASK: %u | FETCHING PERFORMANCE COUNTER IN stop_counting FAILED WITH %i\n", current->pid, read_error);
		if (-EINVAL == (int64_t)read_error) {
			// If this is a per-task event, it must be for current.
			// If this is a per-CPU event, it must be for this CPU.
			printk(KERN_WARNING
				"TASK: %u | EINVAL (-22): Invalid argument! "
				"E.g. trying to measure a different task than itself.\n", current->pid);
		}
	}
	return read_error;
}

u64 terminate_perf_event(struct perf_event* pevent)
{
	u64 result;
	unsigned long irq_flags;

	// disable performance counter while preventing context switching
	local_irq_save(irq_flags);
	perf_event_disable(pevent);
	result = perf_event_release_kernel(pevent);
	local_irq_restore(irq_flags);
    //*pevent = NULL;

	return result;
}


/* -----------------------------------------------------------------------------
 * PID -> pevent* Hashmap
 * Only insertion is needed currently (since memory efficiency is not our
 * current concern)
 */

pid_2_pevent_map pid_2_pevent;

void init_pid_2_pevent_map(void)
{
    int i;
    for (i = 0; i < PROC_BUF_SIZE; ++i) {
        pid_2_pevent.index_to_pid[i] = 0;
        pid_2_pevent.index_to_task_data[i].pevent = NULL;
        pid_2_pevent.index_to_task_data[i].waiting_on_io = 0;
    }
    pid_2_pevent.last_proc_index = 0;
}

/*
 * Returns the pevent-pointer of the corresponding given process by PID if it
 * exists otherwise it returns NULL (assumption: process with PID=0 is not
 * considered a normal process)
 *
 * This is a primitive "Hashmap"-retrieval implementation (O(n))
 */
struct perf_event* get_pevent_by_pid(pid_t pid)
{
    size_t i;
    // to get the pevent by PID one needs to find the index that corresponds to
    // the given PID and use that to retrieve the pevent of the second array
    // directly by index
    size_t proc_index = 0;
    for(i = 0; i < PROC_BUF_SIZE; i++) {
        if (pid_2_pevent.index_to_pid[i] == pid) {
            proc_index = i;
            break;
        }
    }
    return proc_index != 0 ? pid_2_pevent.index_to_task_data[proc_index].pevent : NULL;
}

volatile int* waiting_on_io_by_pid(pid_t pid)
{
    size_t i;
    size_t proc_index = 0;
    for(i = 0; i < PROC_BUF_SIZE; i++) {
        if (pid_2_pevent.index_to_pid[i] == pid) {
            proc_index = i;
            break;
        }
    }
    return proc_index != 0 ? &(pid_2_pevent.index_to_task_data[proc_index].waiting_on_io) : NULL;
}

/*
 * Adds a process (pid, pevent) to the pid->pevent hashmap (NON-idempotently!)
 *
 * Returns if the process has been successfully inserted into the hashmap
 */
int add_proc_to_pevent_map(pid_t pid, struct perf_event* pevent)
{
    if (TASK_BUF_SIZE <= pid_2_pevent.last_proc_index) {
        printk(KERN_WARNING "PROC MAP ADD: last_proc_index too large: %lu\n", pid_2_pevent.last_proc_index);
        return 0;
    }
    printk(KERN_WARNING "i: %lu, pid: %u\n", pid_2_pevent.last_proc_index, pid);
    pid_2_pevent.last_proc_index++;
    pid_2_pevent.index_to_pid[pid_2_pevent.last_proc_index] = pid;
    pid_2_pevent.index_to_task_data[pid_2_pevent.last_proc_index].pevent = pevent;
    return 1;
}

int init_perf_event_into_map(struct task_struct* proc, u64 num_instr)
{
    int error;
    struct perf_event* pevent;
    pid_t pid = proc->pid;

    error = init_perf_event(proc, num_instr, &pevent);
    if (error) {
        printk(KERN_WARNING "TASK: %u | Counting NOT started due to error\n", pid);
        return -1;
    }
    add_proc_to_pevent_map(pid, pevent);

    return 0;
}