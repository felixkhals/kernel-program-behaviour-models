#ifndef __PERF_ERROR_DETECTION_H
#define __PERF_ERROR_DETECTION_H

#include <linux/perf_event.h>

#define TASK_BUF_SIZE 4096
#define PROC_BUF_SIZE 512

int init_perf_event(struct task_struct* proc, u64 num_instr, struct perf_event **pevent);

u64 get_perf_counter(struct perf_event *pevent, u64 *perf_counter);

u64 terminate_perf_event(struct perf_event* pevent);

typedef struct {
    struct perf_event* pevent;
    volatile int waiting_on_io;  // changes to this flag must never be optimized away
} pb_task_data;

typedef struct {
    // primitive int -> int and int -> pevent* hashmap combination
    // the two arrays store the pid and corresponding PBM* at the same index
    pid_t index_to_pid[PROC_BUF_SIZE];
    pb_task_data index_to_task_data[PROC_BUF_SIZE];
    // index of currently last process in the arrays
    size_t last_proc_index;
    //pthread_mutex_t lock;
} pid_2_pevent_map;

void init_pid_2_pevent_map(void);
struct perf_event* get_pevent_by_pid(pid_t pid);
volatile int* waiting_on_io_by_pid(pid_t pid);
int add_proc_to_pevent_map(pid_t pid, struct perf_event* pevent);
int init_perf_event_into_map(struct task_struct* proc, u64 num_instr);

#endif
