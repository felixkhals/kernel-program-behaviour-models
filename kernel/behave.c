#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/perf_event.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/syscalls.h>

#include "behave.h"

bool is_initialized = false;
pid_t root_proc;

/**
 * Set the calling process as the root task of the PBM and initialize it
 */
SYSCALL_DEFINE0(pbm_set_root_proc) {
	unsigned long irq_flags;
    long res;

    // avoid context switching during initialization by disabling interrupts
	local_irq_save(irq_flags);
	if (!is_initialized) {
		pbm_init();
		is_initialized = 1;
	}
	local_irq_restore(irq_flags);

    root_proc = current->pid;
    res = pbm_fork(current, current->real_parent->pid, NULL);

    return 0;
}

/******************************************************************************
* Based on "libpbm" (see header file for more info)
*/

/* -----------------------------------------------------------------------------
 * PID -> PBM* Hashmap
 * Only insertion is needed currently (since memory efficiency is not our
 * current concern)
 */
typedef struct {
    // primitive int -> int and int -> PBM* hashmap combination
    // the two arrays store the pid and corresponding PBM* at the same index
    pid_t index_to_pid[PROC_BUF_SIZE];
    PBM* index_to_pbm[PROC_BUF_SIZE];
    // index of currently last process in the arrays
    size_t last_proc_index;
    //pthread_mutex_t lock;
} shared_pbm_int_map;

shared_pbm_int_map _index_2_pbm;
shared_pbm_int_map* index_2_pbm = &_index_2_pbm;

/**
 * Why not use a generic map data structure using void*, you ask?
 *
 * Because we do not only need _access_ to the data (PBM) but also need _memory-management_ for it.
 * We can not use the generic kernel memory management (e.g. kalloc) since that might need certain
 * tasks to be scheduled which in turn could lead to deadlocks.
 * Therefore memory management/storage and access is implemented by this map in a simple and
 * effective (but certainly inefficient) way.
 */

static void init_pbm_int_map(void) {
    int i;
    //index_2_pbm = init_shared_memory(index_2_pbm, sizeof(shared_pbm_int_map));

    for (i = 0; i < PROC_BUF_SIZE; ++i) {
        index_2_pbm->index_to_pid[i] = 0;
        index_2_pbm->index_to_pbm[i] = NULL;
    }
    index_2_pbm->last_proc_index = 0;
    //init_shared_lock(&index_2_pbm->lock);
}

static void uninit_pbm_int_map(void) {
    //munmap(index_2_pbm, sizeof(index_2_pbm));
}

/*
 * Returns the PBM-pointer of the corresponding given process by PID if it
 * exists otherwise it returns NULL (assumption: process with PID=0 is not
 * considered a normal process)
 *
 * This is a primitive "Hashmap"-retrieval implementation (O(n))
 */
PBM* get_pbm_by_pid(pid_t pid) {
    size_t i;
    // to get the PBM by PID one needs to find the index that corresponds to
    // the given PID and use that to retrieve the PBM of the second array
    // directly by index
    size_t proc_index = 0;
    for(i = 0; i < PROC_BUF_SIZE; i++) {
        if (index_2_pbm->index_to_pid[i] == pid) {
            proc_index = i;
            break;
        }
    }
    return proc_index != 0 ? index_2_pbm->index_to_pbm[proc_index] : NULL;
}

/*
 * Adds a process (pid, pbm) to the pid->pbm hashmap (NON-idempotently!)
 *
 * Returns if the process has been successfully inserted into the hashmap
 */
int add_proc_to_map(pid_t pid, PBM* pbm) {
    //pthread_mutex_lock(&index_2_pbm->lock);
    if (TASK_BUF_SIZE <= index_2_pbm->last_proc_index) {
        printk(KERN_WARNING "PROC MAP ADD: last_proc_index too large: %lu\n", index_2_pbm->last_proc_index);
        return 0;
    }
    printk(KERN_WARNING "i: %lu, pid: %u\n", index_2_pbm->last_proc_index, pid);
    index_2_pbm->last_proc_index++;
    index_2_pbm->index_to_pid[index_2_pbm->last_proc_index] = pid;
    index_2_pbm->index_to_pbm[index_2_pbm->last_proc_index] = pbm;
    //pthread_mutex_unlock(&index_2_pbm->lock);
    return 1;
}

static void debug_print_map(void) {
    size_t i;
    // lock to make the map printing sequential without interleaving other
    // outputs
    //pthread_mutex_lock(&index_2_pbm->lock);
    printk(KERN_WARNING "MAP\n-----\n");
    for(i = 1; i < PROC_BUF_SIZE; i++) {
        if (0 == index_2_pbm->index_to_pid[i])
            break;
        printk(KERN_WARNING "  %u\n", index_2_pbm->index_to_pid[i]);
    }
    printk(KERN_WARNING "-----\n");
    //pthread_mutex_unlock(&index_2_pbm->lock);
}

/* -----------------------------------------------------------------------------
 * Task buffer which holds the nodes of the task graph
 */
typedef struct {
    // buffer that holds the nodes of the task graph
    pbm_NODE task_buffer[TASK_BUF_SIZE];
    // index of current task in task_buffer[]
    uint32_t curr_task_index;
    //pthread_mutex_t lock;
} task_buf;

task_buf _tasks;
task_buf* tasks = &_tasks;

void init_task_buf(void) {
    //tasks = init_shared_memory(tasks, sizeof(task_buf));
    memset(tasks, 0, sizeof(task_buf));
    tasks->curr_task_index = 0;
    //init_shared_lock(&tasks->lock);
}

void uninit_task_buf(void) {
    //munmap(tasks, sizeof(tasks));
}

pbm_NODE* task_alloc(void) {
    pbm_NODE* new_task_node;
    //pthread_mutex_lock(&tasks->lock);
    if (TASK_BUF_SIZE <= tasks->curr_task_index) {
        printk(KERN_WARNING "ERROR: Tried to alloc more tasks than available!\n");
        return NULL;
    }
    // get the memory address of the next free task node space
    new_task_node = &(tasks->task_buffer[tasks->curr_task_index]);
    tasks->curr_task_index++;
    //pthread_mutex_unlock(&tasks->lock);
    return new_task_node;
}

/*
* Get the index of a given task (which can then be used as an unambiguous identificator).
* Returns -1 if given NULL.
*/
static int64_t task_2_index(pbm_NODE* task) {
    int64_t res = ((uint64_t)task - (uint64_t)tasks->task_buffer) / sizeof(pbm_NODE);
    if (!task || res > PROC_BUF_SIZE) {
        return -1;
    } else {
        return res;
    }
}

void debug_print_tasks(void) {
    size_t i;
    pbm_NODE t;
    printk(KERN_WARNING "-----\nTASKS:\n");
    for(i = 0; i <= tasks->curr_task_index; i++) {
        t = tasks->task_buffer[i];
        printk(KERN_WARNING "addr: %lu, type: %u, thread_id: %u, count: %llu,  children: %lli, next_sib: %lli\n",
                i, t.type, t.thread_id, t.count, task_2_index(t.children), task_2_index(t.next_sib));
    }
    printk(KERN_WARNING "^^^^^\n");
}

 /* -----------------------------------------------------------------------------
 * PBM buffer which holds the nodes of the process graph
 */
typedef struct {
    // buffer that holds the nodes of the process graph
    PBM process_buffer[PROC_BUF_SIZE];
    // index of current process in proc_buffer[]
    uint32_t curr_proc_index;
    //pthread_mutex_t lock;
} proc_buf;

proc_buf _procs;
proc_buf* procs = &_procs;

static void init_proc_buf(void) {
    //procs = init_shared_memory(procs, sizeof(proc_buf));
    memset(procs, 0, sizeof(proc_buf));
    procs->curr_proc_index = 0;
    //init_shared_lock(&procs->lock);
}

static void uninit_proc_buf(void) {
    //munmap(procs, sizeof(procs));
}

PBM* proc_alloc(void) {
    PBM* new_pbm;
    //pthread_mutex_lock(&procs->lock);
    if (PROC_BUF_SIZE <= procs->curr_proc_index) {
        printk(KERN_WARNING "ERROR: Tried to alloc more processes than available!\n");
        return NULL;
    }
    printk(KERN_WARNING "alloc proc index: %u\n", procs->curr_proc_index);
    // get the memory address of the next free process node space
    new_pbm = &(procs->process_buffer[procs->curr_proc_index]);
    procs->curr_proc_index++;
    //pthread_mutex_unlock(&procs->lock);
    return new_pbm;
}

/*
* Get the index of a given PBM (which can then be used as an unambiguous identificator)
* Returns -1 if given NULL.
*/
static int64_t pbm_2_index(PBM* pbm) {
    int64_t res = ((uint64_t)pbm - (uint64_t)procs->process_buffer) / sizeof(PBM);
    if (!pbm || res > PROC_BUF_SIZE) {
        return -1;
    } else {
        return res;
    }
}

void debug_print_procs(void) {
    size_t i;
    PBM p;
    printk(KERN_WARNING "-----\nPROCS:\n");
    for(i = 0; i <= procs->curr_proc_index; i++) {
        p = procs->process_buffer[i];
        printk(KERN_WARNING "index: %lu, root: %lli, last: %lli, children: %lli, next_sib: %lli\n",
                // calculate indices instead of using the pointers for more readability and represent NULL-values with -1
                i, task_2_index(p.root), task_2_index(p.last), pbm_2_index(p.children), pbm_2_index(p.next_sib));
    }
    printk(KERN_WARNING "^^^^^\n");
}

/* -----------------------------------------------------------------------------
* General function for management and creation of program behavior models (PBMs)
*/

void pbm_init(void) {
    init_pbm_int_map();
    init_task_buf();
    init_proc_buf();
}

void pbm_uninit(void) {
    uninit_pbm_int_map();
    uninit_task_buf();
    uninit_proc_buf();
}

/* Insert a task node into the PBM of given type (only COMP for now) */
int pbm_task_start(PBM* pbm, uint8_t type, struct task_struct* proc) {
    pbm_NODE* node;
	struct perf_event *pevent;
    int error;

	// only continue for COMP-nodes since other types are not implemented yet
    if(!(pbm && type == COMP)) {
        return 0;
	}

	/*
    * Append a new node to the task graph
    */
    node = _pbm_create_node(type, proc->pid);
    if(!node) {
        printk(KERN_WARNING "ERROR: Could not create node!\n");
        return 0;
    }
    if(!pbm->root) {
        pbm->root = node;
	} else {
        pbm->last->children = node; // append the new node
	}
	// finally the new node becomes the last inserted one
    pbm->last = node;

    error = init_perf_event(proc, 400800, &pevent);
    if (error) {
        printk(KERN_WARNING "TASK: %u | Counting NOT started due to error\n", proc->pid);
        return -1;
    }

    pbm->pevent = pevent;
    printk(KERN_WARNING "TASK: %u | Counting started...\n", proc->pid);
    return 0;
}

/* Conclude the last task of the given PBM */
int pbm_task_end(PBM* pbm) {
    int read_error;
	struct perf_event *pevent;
	u64 perf_counter;

    if (!pbm) {
        printk(KERN_WARNING "Error: Could not end given task due to invalid PBM!\n");
        return 0;
    }

    // record performance results
	if (!is_initialized) {
		printk(KERN_WARNING "TASK: %u | Pevent map not initialized!\n", current->pid);
		return 0;
	}
	printk(KERN_WARNING "TASK: %u | Stopping counting...\n", current->pid);
	pevent = pbm->pevent;
	if (!pevent) {
		printk(KERN_WARNING "TASK: %u | ERROR: Could not find perf_event!\n", current->pid);
		return 0;
	}
	if (IS_ERR(pevent)) {
		printk(KERN_WARNING "TASK: %u | PEVENT INVALID\n", current->pid);
		return 0;
	}
    read_error = get_perf_counter(pevent, &perf_counter);
	if (read_error) {
        printk(KERN_WARNING "TASK: %u | ...something went wront while stopping counting\n", current->pid);
		return -1;
	}
	pbm->last->count = perf_counter;

    terminate_perf_event(pevent);

    printk(KERN_WARNING "TASK: %u | ...Counting stopped: %llu instr.\n", current->pid, perf_counter);

    return 0;
}

/**
 * Why is the code concerning the forking separated into the two functions
 * pbm_fork_parent_new_task() and pbm_fork() instead of simply putting it at the end of _do_fork?
 *
 * The separation is necessary since in the _do_fork a context switch from the parent to the
 * child process takes place which is problematic since we want to end (and restart) perf-measuring
 * the parent as well as the child process and the measurements (in pbm_task_end()) can only happen
 * from the process itself. But in the beginning of _do_fork the child process does not exist yet.
 * Therefore we have to split the code into the two functions to be able to measure the parent
 * before the context switch as well as initialize the child-measuring after switching to the child.
 */

/* Stop previous task and start new task for the parent process and also reset the perf counter
 * Returns a pointer to the fork-task-node which the forked process can use as a time information.
 *
 * BEWARE:
 * Inside the _do_fork routine the context gets switched from the parent to the child process.
 * This function must get called in the _do_fork() routine BEFORE (!) the child process starts to
 * run (current == parent) otherwise the perf counting will fail!
 */
 pbm_NODE* pbm_fork_parent_new_task(struct task_struct* parent_proc) {
    PBM* parent_pbm;
    pbm_NODE* fork_node;

    // end task of parent process
	parent_pbm = get_pbm_by_pid(parent_proc->pid);
	if(!parent_pbm) {
        // this should only happen right at the first call to pbm_fork() since at this point no
        // parent process has been initialized yet since this is the first relevant parent process
        if (current->pid != root_proc) {
            printk(KERN_WARNING "ERROR: COULD NOT FIND PARENT-PBM!\n");
            BUG();
        }
        return NULL;
	}
	pbm_task_end(parent_pbm);

	/*
    * Before starting the new task, append the fork-node to the task graph to maintain the correct order
    */
    printk(KERN_WARNING ">> CREATE NEW FORK NODE! by %u\n", parent_proc->pid);
    fork_node = _pbm_create_node(FORK, parent_proc->pid);
    if(!fork_node) {
        printk(KERN_WARNING "COULD NOT CREATE NEW FORK NODE!\n");
        return NULL;
    }
    if(!parent_pbm->root) {
        parent_pbm->root = fork_node;
    } else {
        parent_pbm->last->children = fork_node; // append the new node
    }
    parent_pbm->last = fork_node; // the new node becomes the last inserted one

    // any fork-node has exactly two children because a fork creates only one
    // copy of an existing process (1 (copy) + 1 (existing) = 2)
    fork_node->count = 2;

    // start the new task
	pbm_task_start(parent_pbm, COMP, parent_proc);

    return fork_node;
 }

//TODO Consider the difference between calling it from the root-process (that has no registered parent process) and from "normal" child processes (that are registered in the maps)
/* Insert a FORK node into the given PBM for up to 'num_thr' child threads
 *
 * BEWARE:
 * Inside fork.c:_do_fork() the context gets switched from the parent to the child process.
 * This function must get called in the _do_fork() routine AFTER (!) the child process starts to
 * run (current == child) otherwise the perf counting will fail!
 */
int pbm_fork(struct task_struct* proc, pid_t parent_pid, pbm_NODE* fork_node) {

	PBM* parent_pbm;
    PBM* child_pbm;

    /* NOTE:
     * since the first time that _do_fork() is called the parent is "bash" which causes the
     * pbm_fork_parent_new_task() to not be called but pbm_fork() since the child is "mpirun" so we
     * put the initialization in here instead of into pbm_fork_parent_new_task().
     */

	child_pbm = get_pbm_by_pid(proc->pid);
	parent_pbm = get_pbm_by_pid(parent_pid);

    printk(KERN_WARNING "FORK: %u from parent %u\n", proc->pid, parent_pid);

    // check if the child already exists (if and only if the "forked" process
    // is the process itself which happens in this case because OpenMP also
    // uses the parent process for parallel calculations)
    if (!child_pbm) {
        // Create and initialize a new PBM for the child
        {
            child_pbm = proc_alloc();
            if(!child_pbm) {
                printk(KERN_WARNING "ERROR: Could not alloc child-PBM! %llx\n", (uint64_t)&child_pbm);
                return 0;
            }

            child_pbm->root = NULL;
            child_pbm->last = NULL;
            child_pbm->children = NULL;
            child_pbm->next_sib = NULL;
            child_pbm->fork_date = NULL; // this gets updated later in the function // TODO Remove initialization here since it is done later?
            child_pbm->exit_date = NULL;
        }
        if (!add_proc_to_map(proc->pid, child_pbm)) {
            printk(KERN_WARNING "FORK ERROR: Could not add process to map: %u\n", proc->pid);
            return 0;
            // TODO Reverse previous allocation of child pbm?
        } else {
            printk(KERN_WARNING "Added process to map: %u\n", proc->pid);
            debug_print_map(); //FIXME
        }
    } else {
        printk(KERN_WARNING "Process already exists: %u\n", proc->pid);
    }

    // add child pbm to parents children
	if(parent_pbm) { // checking this is only important in case of the root task which has no (recorded) parent
		if(parent_pbm->children) {
			// prepend the child to the list of children so we dont have to
			// modify the sibbling (which works since we use a single-linked
			// list)
			child_pbm->next_sib = parent_pbm->children;
		}
		parent_pbm->children = child_pbm;
	} else {
        BUG_ON(current->pid != root_proc);
    }

    // We have to know WHEN the fork happens relative to the parent. So every child remembers the
    // current fork-task-node of the parent on exit (so that the merge can happen at the correct
    // position (more or less, may be imperfect due to parallelism))
    child_pbm->fork_date = fork_node;

    // continue performance counting for child (restarting parent counting has already been started
    pbm_task_start(child_pbm, COMP, proc);

    // insert front of child task graph into parent task graph:
    // prepend child-task-tree to list of child-nodes in the fork-node
    // preconditions:
    //      * child_pbm->root->next_sib == NULL: since any fork-node has at most two children and
    //        only one before the join-operation)
    //      * child_pbm->root != NULL: since the previously called pbm_task_start() should have
    //        written to that
    if (fork_node) {
        child_pbm->root->next_sib = fork_node->children;
        fork_node->children = child_pbm->root;
    }

    return 1;
}

// This should get called by the child at sysexit()
int pbm_exit(pid_t pid, pid_t parent_pid) {
    PBM* pbm;
    PBM* parent_pbm;

    printk(KERN_WARNING "EXIT: %u\n", pid);
    pbm = get_pbm_by_pid(pid);
    if(!pbm) {
        printk(KERN_WARNING "COULD NOT FIND PBM!\n");
        debug_print_map();
        return 0;
    }
    pbm_task_end(pbm);

    parent_pbm = get_pbm_by_pid(parent_pid);
    // set current parent task as the exit task of this child where the join
    // gets inserted
    if(parent_pbm) {
        if (parent_pbm->last == NULL) {
            printk(KERN_WARNING "ERROR: ParentPBM->LAST is NULL! ppid: %u\n", parent_pid);
        }
        pbm->exit_date = parent_pbm->last;
    } else {
        if (current->pid != root_proc) {
            printk(KERN_WARNING "ERROR: COULD NOT FIND PARENT! child: %u, parent: %u\n", pid, parent_pid);
            BUG();
        }
    }

    return 1;
}

/* -----------------------------------------------------------------------------
* PBM graph post-processing functions
*/

/* Insert a JOIN node into the given PBM and merge the forked child PBMs into this PBM */
int pbm_join(PBM* child_pbm) {
    pbm_NODE* fork_node;
    pbm_NODE* join_node;
    pid_t join_label;

    if(!child_pbm)
        return 0;

    if (!child_pbm->exit_date) {
        printk(KERN_WARNING "Exit date missing for pid %u. Has the process really been exited?\n", child_pbm->last->thread_id);
    }

    printk(KERN_WARNING "Joining thread %u with fork_node: %lli\n", child_pbm->last->thread_id, task_2_index(child_pbm->fork_date));
    fork_node = child_pbm->fork_date;

    // the child process is used to label the join operation to know which process the join belongs
    // to since using the parent as the label would be ambiguous since more than one child could
    // have been spawned by the same parent
    join_label = child_pbm->last->thread_id;
    join_node = _pbm_create_node(JOIN, join_label);
    if(!join_node) {
        printk(KERN_WARNING "ERROR: Could not create node!\n");
        return 0;
    }

    // any join-node has exactly two children because it is reversing a fork
    // which creates only one copy of an existing process (1 (copy) + 1 (existing) = 2)
    join_node->count = 2;

    // insert back of child task graph with appended join-node into parent task graph
    // precondition:
    //      * child_pbm->last->children == NULL: since it should be the last task the child did
    //        before exit
    // append join node to child task-graph
    child_pbm->last->children = join_node;
    // insert join node directly after the exit-date-node in the parent pbm
    printk(KERN_WARNING "Exit-date: %lli, child: %lli\n", task_2_index(child_pbm->exit_date), task_2_index(child_pbm->exit_date->children));
    join_node->children = child_pbm->exit_date->children;
    child_pbm->exit_date->children = join_node;

    return 1;
}

// recursively traverse all PBMs and insert the child task-graphs
void pbm_post_processing(PBM* pbm) {
    PBM* sib_pbm;
    PBM* child_pbm = pbm->children;
    if(child_pbm) {
        printk(KERN_WARNING "Joining child thread %u\n", child_pbm->last->thread_id);
        pbm_post_processing(child_pbm);
        //pbm_join(child_pbm);
        // TODO Remove from list of childs or just mark as visited?
    }

    sib_pbm = pbm->next_sib;
    if(sib_pbm) {
        printk(KERN_WARNING "Joining sib thread %u\n", sib_pbm->last->thread_id);
        pbm_post_processing(sib_pbm);
        //pbm_join(sib_pbm);
        // TODO Remove from list of siblings or just mark as visited?
    }
}

/* Crude recursive pbm node counter, starts with given PBM */
u64 pbm_count_children(PBM* pbm) {
    printk(KERN_WARNING "Node %lli: (pid = %u)\n", pbm_2_index(pbm), pbm->root->thread_id);
    // count itself (1) and add the count of children and sibblings if existing
    return 1 + (pbm->children ? pbm_count_children(pbm->children) : 0)
             + (pbm->next_sib ? pbm_count_children(pbm->next_sib) : 0);
}

/* -----------------------------------------------------------------------------
* PBM graph output functions
*/

/**
 * BEWARE:
 * This function must not be called before ALL childs, grandchilds etc. of the root process of a
 * measured program are exited since otherwise the informations in the model are not complete and
 * therefore this function will throw an error!
 * In this case most probably pbm_join() will generate the error since the exit_date of the not yet
 * exited process will not be set and also the perf counter values will probably be wrong.
 */
void pbm_join_and_print_graph_self(pid_t pid) {
    PBM* pbm;

    debug_print_map();
    debug_print_procs();
    debug_print_tasks();
    printk(KERN_WARNING "indices: %lu, %u, %u\n", index_2_pbm->last_proc_index, procs->curr_proc_index, tasks->curr_task_index);
    pbm = get_pbm_by_pid(pid);
    if (pbm) {
        pbm->child_count = pbm_count_children(pbm);
        printk(KERN_WARNING "Child count: %llu\n", pbm->child_count);
        pbm_post_processing(pbm);
        printk(KERN_WARNING "After post-processing:\n");
        debug_print_tasks();
        pbm_print_graph(pbm, pbm->root);
    } else {
        printk(KERN_WARNING "JOIN: PBM not found for: %u\n", pid);
    }
    // reset so that is_relevant_process() can return early
    is_initialized = 0;

    printk(KERN_WARNING "ROOT: %u\n", pid);
}

/* Crude recursive ADG printer, starts with given node */
void pbm_print_graph(PBM* pbm, pbm_NODE* node) {
    pbm_NODE* root;
    char types[5][5] = {"", "FORK", "JOIN", "COMP", "COMM"};

    if(!node)
        return;

    if(node->visited)
        return;

    root = node;
    printk(KERN_WARNING "Node %lli: (%s, pid = %u, count = %llu), children:\n",
            task_2_index(node), types[node->type], node->thread_id, node->count);

    if(node->children)
    {
        node = node->children;
        while(node)
        {
            printk(KERN_WARNING "  -- Node %lli: (%s, pid = %u, count = %llu), next sibling: %lli\n",
                    task_2_index(node), types[node->type], node->thread_id, node->count, task_2_index(node->next_sib));
            node = node->next_sib;
        }
    } else {
        printk(KERN_WARNING "  -- Exit (pid = %u)\n", node->thread_id);
    }

    if(root->children)
        pbm_print_graph(pbm, root->children);

    if(root->next_sib)
        pbm_print_graph(pbm, root->next_sib);

    root->visited = 1;
    if(root == pbm->root)
        _pbm_unvisit_node(pbm->root);
}

/* -----------------------------------------------------------------------------
* Auxiliary functions, not for public use.
*/

pbm_NODE* _pbm_create_node(uint8_t type, pid_t pid) {
    pbm_NODE* node = task_alloc();
    if(!node)
        return NULL;

    node->thread_id = pid;
    node->type = type;
    node->count = 0;
    node->children = NULL;
    node->next_sib = NULL;
    node->visited = 0;
    return node;
}

// recursive
void _pbm_unvisit_node(pbm_NODE* node) {
    if(!node)
       return;

    if(!node->visited)
        return;

    if(node->children)
        _pbm_unvisit_node(node->children);

    if(node->next_sib)
        _pbm_unvisit_node(node->next_sib);

    node->visited = 0;
}

/******************************************************************************/

bool is_root_process(struct task_struct* p) {
    return p->pid == root_proc
            // make sure that no process is accidentally (before init) declared root
            && is_initialized;
}

bool is_relevant_process(struct task_struct* p) {
    struct task_struct* proc = p;
    // return early if init is not done since there can be no relevant processes
    if (!is_initialized) {
        return false;
    }
    // check if mpirun is a parent, super-parent, ... until the linux root/idle-process
    // (comm = "swapper", pid = 0) is found
    while (proc->pid != 0) {
        if (is_root_process(proc)) {
            return true;
        }
        proc = proc->real_parent;
    }
    return false;
}


/******************************************************************************
 * Plan runtime state
 */
struct plan_rt_state prs;

void plan_rt_state_init(void)
{
    int i;
    for (i = 0; i < PROC_BUF_SIZE; ++i) {
        prs.node_stack[i] = NULL;
        prs.proc_stack[i] = NULL;
    }
    prs.stack_size = 0;
    prs.num_exited_procs = 0;
}

struct task_struct* plan_rt_state_peek_proc(void)
{
    return prs.proc_stack[prs.stack_size - 1];
}

void plan_rt_state_peek(pbm_NODE** node_res, struct task_struct** proc_res)
{
	if (0 != prs.stack_size) {
		*node_res = prs.node_stack[prs.stack_size - 1];
		*proc_res = plan_rt_state_peek_proc();
	}
}

int plan_rt_state_is_empty(void)
{
	// it is sufficient to only check for the process since there should never be a process without
	// a corresponding node and vice verca
	return NULL == plan_rt_state_peek_proc();
}

void plan_rt_state_push(pbm_NODE* node, struct task_struct* proc)
{
    if (prs.stack_size >= PROC_BUF_SIZE) {
        printk(KERN_WARNING "ERROR: Plan runtime stack is full!\n");
        return;
    }
    prs.node_stack[prs.stack_size] = node;
    prs.proc_stack[prs.stack_size] = proc;
    prs.stack_size++;
}

void plan_rt_state_pop(void)
{
    if (prs.stack_size == 0) {
        printk(KERN_WARNING "ERROR: Plan runtime stack is already empty!\n");
    }
    prs.stack_size--;
    prs.node_stack[prs.stack_size] = NULL;
    prs.proc_stack[prs.stack_size] = NULL;
}

void plan_rt_state_incr_num_exited_procs(void)
{
    prs.num_exited_procs++;
}

u64 plan_rt_state_num_exited_procs(void)
{
    return prs.num_exited_procs;
}

void plan_rt_state_debug_print(void)
{
    size_t i;
    pbm_NODE* node;
    struct task_struct* task;
    printk(KERN_WARNING "MAP\n-----\n");
    for(i = 0; i < prs.stack_size; i++) {
        node = prs.node_stack[i];
        task = prs.proc_stack[i];
        printk(KERN_WARNING "[%lu] %p, %p (%i, %s)\n", i, node, task, task ? task->pid : 0, task ? task->comm : "-");
    }
    printk(KERN_WARNING "-----\n");
}
