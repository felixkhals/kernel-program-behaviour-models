#!/bin/bash

# update and install without authentication checking since the old certificates
# have run out anyways and it would otherwise prevent the update process from working.
# BEWARE: I KNOW THIS IS DANGEROUS AND UGLY BUT I DID NOT FIND A BETTER SOLUTION!
# (idea: https://unix.stackexchange.com/questions/317695/is-it-possible-to-have-apt-accept-an-invalid-certificate/698834#698834)
apt -o "Acquire::https::Verify-Peer=false" update
apt-get install -y \
	--force-yes -o "Acquire::https::Verify-Peer=false" `# ignore authentication problems` \
	fakeroot \
	build-essential \
	ncurses-dev \
	xz-utils \
	libssl-dev \
	bc \
	flex \
	libelf-dev bison \
	`# install MPI` \
	openmpi-bin \
	libopenmpi-dev \
	ssh `# ssh is needed for OMPI (see https://serverfault.com/questions/609894/run-an-mpi-application-in-docker/699846#699846)`

cd /mnt
./build_kernel.sh
